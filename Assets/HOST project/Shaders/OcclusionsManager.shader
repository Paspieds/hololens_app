﻿// Upgrade NOTE: replaced 'UNITY_INSTANCE_ID' with 'UNITY_VERTEX_INPUT_INSTANCE_ID'

Shader "PostProcess/OcclusionsManager"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }

    SubShader
    {
       Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
       // Tags { "RenderType" = "Transparent" }
       /* Blend SrcAlpha OneMinusSrcAlpha
       ZWrite off */

       Pass
       {
          CGPROGRAM

          #pragma vertex vert
          #pragma fragment frag

          #include "UnityCG.cginc"

          struct appdata
          {
              float4 vertex : POSITION;
              float2 uv : TEXCOORD0;
              UNITY_VERTEX_INPUT_INSTANCE_ID
          };
          struct v2f
          {
              float2 uv : TEXCOORD0;
              float4 vertex : SV_POSITION;
              UNITY_VERTEX_INPUT_INSTANCE_ID
              UNITY_VERTEX_OUTPUT_STEREO
          };

          v2f vert(appdata v)
          {
              v2f o;
              UNITY_SETUP_INSTANCE_ID(v);
              UNITY_TRANSFER_INSTANCE_ID(v, o);
              UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
              o.vertex = UnityObjectToClipPos(v.vertex);
              o.uv = v.uv;
              return o;
          }

          UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);

          fixed4 frag(v2f i) : SV_Target
          {
              UNITY_SETUP_INSTANCE_ID(i);
              fixed4 col = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv);
              
              float distFromCenter = distance(i.uv.xy, float2(0.5, 0.5));

              if (distFromCenter <= 0.2) {
                  col = fixed4(0, 0, 0, 0);
              }
              /*else if (distFromCenter <= 0.45   ) {

                  float alpha = distFromCenter - 0.1;
                  if (alpha < 0.0) {
                      alpha = 0.0;
                  }

                  col = fixed4(col.r, col.g, col.b, alpha);
              }*/

              return col;
          }
          ENDCG
       }
    }
}