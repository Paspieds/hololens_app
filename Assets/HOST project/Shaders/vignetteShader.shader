﻿Shader "Unlit/Unlit Vignette Alpha"
{
	Properties
	{
		_Color("Main Color", Color) = (1, 1, 1, 1)
		_MainTex("Texture", 2D) = "white" {}
		_Intensity("Intensity", Range(0.0, 6.0)) = 5.5
		_Tiling("Tiling", Vector) = (1, 1, 0, 0)
	}
		SubShader
		{
			Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
			LOD 100

			ZWrite Off
			// Blend SrcAlpha OneMinusSrcAlpha

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				// make fog work
				#pragma multi_compile_fog

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float2 uv : TEXCOORD0;
					UNITY_FOG_COORDS(1)
					float4 vertex : SV_POSITION;
				};

				uniform float _Intensity;
				uniform float4 _Tiling;
				uniform sampler2D _MainTex;
				uniform float4 _MainTex_ST;

				float4 _Color;

				float getVignette(float2 texCoord)
				{
					texCoord -= 0.5;
					float vignette = 1.0 - pow(dot(texCoord, texCoord),1);

					vignette = 1 - pow(vignette,_Intensity); //determine vignette amount

					return vignette;
				}

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = TRANSFORM_TEX(v.uv, _MainTex);
					UNITY_TRANSFER_FOG(o,o.vertex);
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 color = tex2D(_MainTex, i.uv * _Tiling.xy) * _Color;

					/*if (i.uv.x < 0.1) {
						color = fixed4(0, 0, 0, 0);
					}*/

					float distFromCenter = distance(i.uv.xy, float2(0.5, 0.5));
					if (distFromCenter <= 0.2) {
						color = fixed4(0, 0, 0, 0);
					}
					else {
						float vignette = getVignette(i.uv);
						color.a *= vignette;
					}

					return color;
				}
				ENDCG
			}
		}
}