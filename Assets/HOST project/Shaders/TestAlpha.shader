﻿Shader "Custom/AlphaShader"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }

    SubShader
    {
        Tags{ "RenderType" = "Transparent" "Queue" = "Transparent"}

        Pass
        {

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            float _Distance;
            float _FadeStartDistance;
            float _FadeCompleteDistance;

            sampler2D _MainTex;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

                float distFromCenter = distance(i.uv.xy, float2(0.5, 0.5));

                float alpha = distFromCenter - 0.1;
                if (distFromCenter <= 0.2) {
                    alpha = 0.0;
                }
                else if (distFromCenter <= 0.45) {
                    if (alpha < 0.0) {
                        alpha = 0.0;
                    }
                }

                return col;

                /*if (_Distance > _FadeStartDistance) {
                    return fixed4(1, 1, 1, 1); // no change
                }
                if (_Distance > _FadeCompleteDistance) {
                    return fixed4(1, 1, 1, (_Distance - _FadeCompleteDistance) / (_FadeStartDistance - _FadeCompleteDistance)); // fading out
                }
                return fixed4(1, 1, 1, 0); // faded out. Opacity = 0*/
            }
            ENDCG
        }
    }
}
