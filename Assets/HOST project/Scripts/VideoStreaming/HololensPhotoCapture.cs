﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Windows.WebCam;
using Photon.Pun;
using Photon.Realtime;
using System.Collections.Generic;

namespace HostProject.VideoStreaming
{
    public class HololensPhotoCapture
    {
        private PhotoCapture photoCaptureObject = null;

        private CameraParameters cameraParameters;

        public PhotonView photonView;
        public Player sender;

        private Resolution cameraResolution;
        public HololensPhotoCapture(PhotonView photonView, Player sender)
        {
            this.photonView = photonView;
            this.sender = sender;

            cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => (res.width / 8) * (res.height / 8)).First();

            Debug.Log("initializing photo taker");

            // Create a PhotoCapture object
            PhotoCapture.CreateAsync(true, delegate (PhotoCapture captureObject) {
                photoCaptureObject = captureObject;
                cameraParameters = new CameraParameters();
                cameraParameters.hologramOpacity = 1.0f;

                cameraParameters.cameraResolutionWidth = cameraResolution.width / 8;
                cameraParameters.cameraResolutionHeight = cameraResolution.height / 8;
                cameraParameters.pixelFormat = CapturePixelFormat.BGRA32;

                Debug.Log("take photo");
                // Activate the camera
                captureObject.StartPhotoModeAsync(cameraParameters, delegate (PhotoCapture.PhotoCaptureResult result) {
                    // Take a picture
                    photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
                });
            });
        }

        void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
        {
            Texture2D targetTexture = new Texture2D(cameraResolution.width / 8, cameraResolution.height / 8);
            Debug.Log("photo taken");
            photoCaptureFrame.UploadImageDataToTexture(targetTexture);
            Debug.Log("=> send texture");
            photonView.RPC("SetTexture", sender, targetTexture.EncodeToPNG());

            photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
        }

        void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
        {
            // Shutdown the photo capture resource
            photoCaptureObject.Dispose();
            photoCaptureObject = null;
        }
    }
}