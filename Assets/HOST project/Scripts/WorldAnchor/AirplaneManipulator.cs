﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.SpatialAwareness;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using UnityEngine;

public class AirplaneManipulator : MonoBehaviour
{

    private IMixedRealitySpatialAwarenessMeshObserver spatialAwarenessMeshObserver = null;

    private void Start()
    {
        // Get the first Mesh Observer available, generally we have only one registered
        spatialAwarenessMeshObserver = CoreServices.GetSpatialAwarenessSystemDataProvider<IMixedRealitySpatialAwarenessMeshObserver>();
        if (Application.isEditor)
        {
            print("We are running this from inside of the editor!");
            GetComponent<SolverHandler>().TrackedTargetType = TrackedObjectType.Head;
        }
    }

    public void StartPlacing()
    {
        if (spatialAwarenessMeshObserver != null)
        {
            spatialAwarenessMeshObserver.DisplayOption = SpatialAwarenessMeshDisplayOptions.Visible;
        }
    }

    public void EndPlacing()
    {
        if (spatialAwarenessMeshObserver != null)
        {
            spatialAwarenessMeshObserver.DisplayOption = SpatialAwarenessMeshDisplayOptions.None;
        }
    }
}
