﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyInEditor : MonoBehaviour
{
    [SerializeField]
    GameObject uiUsedOnlyInEditor;

    // Start is called before the first frame update
    void Start()
    {
        if (!Application.isEditor)
        {
            Destroy(uiUsedOnlyInEditor);
        }
    }
}
