﻿using System;
using System.Collections;
using UnityEngine;

public static class MonoBehaviourExtensions
{
    /// <summary>
    /// Invoke method after delay
    /// </summary>
    /// <param name="monoBehaviour"></param>
    /// <param name="method">Method to invoke</param>
    /// <param name="delay">Delay</param>
    public static void InvokeAfterDelay(this MonoBehaviour monoBehaviour, Action method, float delay) 
        => monoBehaviour.StartCoroutine(InvokeWithDelayRoutine(method, delay));

    /// <summary>
    /// Invoke method after delay routine
    /// </summary>
    /// <param name="method">Method to invoke</param>
    /// <param name="delay">Delay</param>
    /// <returns></returns>
    static IEnumerator InvokeWithDelayRoutine(Action method, float delay)
    {
        yield return new WaitForSeconds(delay);
        method();
    }
}
