﻿using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;

namespace HostProject.Network
{
    public class RoomManager : MonoBehaviour, IConnectionCallbacks, IMatchmakingCallbacks
    {
        #region Private Serializable Fields
        [SerializeField]
        string roomName = "HOST";
        [SerializeField]
        int maxPlayer = 10;
        [SerializeField]
        PhotonServerSettings PhotonServerSettings;

        #endregion


        #region Private Fields


        /// <summary>
        /// This client's version number. Users are separated from each other by gameVersion (which allows you to make breaking changes).
        /// </summary>
        string gameVersion = "1";
        float RetryDelay = 3.0f;


        #endregion


        #region MonoBehaviour CallBacks


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
        /// </summary>
        void Awake()
        {
            // #Critical
            // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
            PhotonNetwork.AutomaticallySyncScene = false;
        }


        /// <summary>
        /// MonoBehaviour method called on GameObject by Unity during initialization phase.
        /// </summary>
        void Start()
        {
            Connect();
        }


        #endregion


        #region Public Methods


        /// <summary>
        /// Start the connection process.
        /// - If already connected, we attempt joining a random room
        /// - if not yet connected, Connect this application instance to Photon Cloud Network
        /// </summary>
        public void Connect()
        {
            // we check if we are connected or not, we join if we are , else we initiate the connection to the server.
            if (PhotonNetwork.IsConnected)
            {
                TryJoinRoom();
            }
            else
            {
                // #Critical, we must first and foremost connect to Photon Online Server.
                PhotonNetwork.ConnectUsingSettings();
                PhotonNetwork.GameVersion = gameVersion;
            }
        }

        void TryJoinRoom()
        {
            PhotonNetwork.NickName = SystemInfo.deviceName;

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsVisible = false;
            roomOptions.MaxPlayers = (byte)maxPlayer;

            ExitGames.Client.Photon.Hashtable details = new ExitGames.Client.Photon.Hashtable();
            
            details.Add("ip", GetLocalIPAddress());

            PhotonNetwork.SetPlayerCustomProperties(details);

            PhotonNetwork.JoinRoom(roomName);

        }


        #endregion

        string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }

        #region Unity Callbacks

        void OnEnable()
        {
            PhotonNetwork.AddCallbackTarget(this);
        }
        void OnDisable()
        {
            PhotonNetwork.RemoveCallbackTarget(this);
        }

        void OnApplicationQuit()
        {
            if (PhotonNetwork.CurrentRoom != null)
            {
                PhotonNetwork.LeaveRoom();
            }
        }

        #endregion

        #region IConnectionCallbacks

        public void OnConnected()
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - Connected");
        }

        public void OnConnectedToMaster()
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - ConnectedToMaster");
            TryJoinRoom();
        }

        public void OnDisconnected(DisconnectCause cause)
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - Disconnected: " + cause);
            // Try another connection
            this.InvokeAfterDelay(Connect, RetryDelay);
        }

        public void OnRegionListReceived(RegionHandler regionHandler)
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - RegionListReceived: " + regionHandler.SummaryToCache);
        }

        public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - CustomAuthenticationResponse");
        }

        public void OnCustomAuthenticationFailed(string debugMessage)
        {
            Debug.Log("[RoomManager|IConnectionCallbacks] - CustomAuthenticationFailed: " + debugMessage);
        }

        #endregion


        #region IMatchmakingCallbacks

        public void OnFriendListUpdate(List<FriendInfo> friendList)
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - FriendListUpdate:");
            foreach (FriendInfo f in friendList)
            {
                Debug.Log(f.UserId);
            }
        }

        public void OnCreatedRoom()
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - CreatedRoom");
        }

        public void OnCreateRoomFailed(short returnCode, string message)
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - CreateRoomFailed (return code " + returnCode + "): " + message);
        }

        public void OnJoinedRoom()
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - JoinedRoom");
        }

        public void OnJoinRoomFailed(short returnCode, string message)
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - JoinRoomFailed, try in " + RetryDelay.ToString());
            // Try another connection
            this.InvokeAfterDelay(Connect, RetryDelay);
        }

        public void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - JoinRandomFailed (return code " + returnCode + "): " + message);
        }

        public void OnLeftRoom()
        {
            Debug.Log("[RoomManager|IMatchmakingCallbacks] - LeftRoom");
        }

        #endregion
    }
}