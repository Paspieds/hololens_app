﻿using HostProject.VideoStreaming;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Threading;
using TMPro;
using UnityEngine;

public class PhotonView1RPC : MonoBehaviour
{
    #region Private serialized fields

    [Header("Message notification")]
    [SerializeField]
    GameObject MessageText;
    [Header("Virtual events")]
    [SerializeField]
    GameObject lockHelpText;
    [SerializeField]
    GameObject arrowBag;
    [SerializeField]
    Animator animatorWoman;
    [Header("Audio notifications")]
    [SerializeField]
    AudioClip noticationSound;
    [SerializeField]
    AudioClip pilotAnnouncement;
    [SerializeField]
    AudioClip stressfulNoise;
    [Header("Other")]
    [SerializeField]
    GameObject worldAnchor;

    #endregion

    #region Private fields

    AudioSource audioSource;

    private PhotonView photonView;
    private bool timerActive = true;
    private HololensPhotoCapture photoTaker;

    #endregion

    private void Start()
    {
        photonView = PhotonView.Get(this);
        audioSource = GetComponent<AudioSource>();
    }

    #region Photon RPCs

    /// <summary>
    /// Message event triggered by the moderation app
    /// </summary>
    /// <param name="scenario">Scenario of the message event</param>
    /// <param name="type">Type of message</param>
    /// <param name="content">Content of the message</param>
    [PunRPC]
    public void TriggerMessageEvent(int scenario, string type, string content) {
        Debug.Log("[PhotonViewRPC1] - TriggerMessageEvent");
        StartCoroutine(ShowMessageForPeriod(content, 5.0f));
    }

    /// <summary>
    /// Virtual event triggered from the moderation app
    /// </summary>
    /// <param name="scenario">Scenario of the virtual event</param>
    /// <param name="actionNumber">Action number linked with the virtual event</param>
    [PunRPC]
    public void TriggerVirtualEvent(int scenario, string actionNumber)
    {
        switch (actionNumber)
        {
            case "1":
                StartCoroutine(MakeWomanStandUp(10.0f));
                break;
            case "2":
                audioSource.clip = pilotAnnouncement;
                audioSource.Play();
                break;
            case "3":
                audioSource.clip = stressfulNoise;
                audioSource.Play();
                break;
        }
    }

    [PunRPC]
    public void TriggerHelpEvent(int scenario, string actionNumber)
    {
        switch (actionNumber)
        {

            case "1":
                TriggerHintLock(10.0f);
                break;
            case "2":
                ShowArrowbag(10.0f);
                break;
        }
    }

    /// <summary>
    /// Trigger from the moderation app, it stops the simulation
    /// </summary>
    [PunRPC]
    public void StopSimulation()
    {
        //appLogic.PlaceableWorldAnchor.HideDecor();
        worldAnchor.SetActive(false);
        StartCoroutine(ShowMessageForPeriod("Simulation over", 10.0f));

        Debug.Log("Stop timer event and photo taker");
        timerActive = false;
    }

    [PunRPC]
    public void StartStreamingPhotoCapture(PhotonMessageInfo info){
        Debug.Log("StartStreamingPhotoCapture 1");
        StartCoroutine(SendImagesToModerator(info.Sender));
    }

    #endregion

    #region Private methodes



    private IEnumerator TakeAndSendPhoto(Player p)
    {
        new HololensPhotoCapture(photonView, p);
        yield return new WaitForSeconds(0.5f); // wait half a second
    }


    private IEnumerator SendImagesToModerator(Player p)
    {
        // send screenshots continuously
        while (timerActive)
        {
            new HololensPhotoCapture(photonView, p);
            yield return new WaitForSeconds(0.5f); // wait half a second
        }
    }

    /// <summary>
    /// Show a message for a duration
    /// </summary>
    /// <param name="content">Content of the message</param>
    /// <param name="time">Duration</param>
    /// <returns></returns>
    IEnumerator ShowMessageForPeriod(string content, float time)
    {
        MessageText.GetComponent<TextMeshPro>().text = content;
        MessageText.SetActive(true);
        TriggerNotificationSound();
        yield return new WaitForSeconds(time);
        MessageText.SetActive(false);
    }

    /// <summary>
    /// Trigger the passenger standing animation
    /// </summary>
    /// <param name="timeStanding">Duration where the woman stands</param>
    /// <returns></returns>
    IEnumerator MakeWomanStandUp(float timeStanding)
    {
        animatorWoman.SetTrigger("STAND");
        yield return new WaitForSeconds(timeStanding);
        animatorWoman.SetTrigger("SIT");
    }

    /// <summary>
    /// Trigger the notification sound
    /// </summary>
    void TriggerNotificationSound()
    {
        audioSource.clip = noticationSound;
        audioSource.Play();
    }

    /// <summary>
    /// Trigger the animation with the lock
    /// </summary>
    /// <param name="displayTime">Duration</param>
    private void TriggerHintLock(float displayTime)
    {
        StartCoroutine(ShowGameObjectForPeriod(lockHelpText, displayTime));
    }

    /// <summary>
    /// Show a GameObject for a duration
    /// </summary>
    /// <param name="go">GameObject to show</param>
    /// <param name="duration">Duration</param>
    /// <returns></returns>
    IEnumerator ShowGameObjectForPeriod(GameObject go, float duration)
    {
        go.SetActive(true);
        TriggerNotificationSound();
        yield return new WaitForSeconds(duration);
        go.SetActive(false);
    }

    /// <summary>
    /// Trigger the arrow pointing down at a bag
    /// </summary>
    /// <param name="displayTime">Duration</param>
    private void ShowArrowbag(float displayTime)
    {
        StartCoroutine(ShowGameObjectForPeriod(arrowBag, displayTime));
    }

    #endregion
}
