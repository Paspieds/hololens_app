﻿using HostProject.VideoStreaming;
using Microsoft.MixedReality.Toolkit;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using TMPro;
using UnityEngine;

public class PhotonView2RPC : MonoBehaviour
{
    #region Private Fields

    private PhotonView photonView;

    #endregion

    #region Private serialized fields

    [SerializeField]
    GameObject hostnameText;
    [SerializeField]
    GameObject instructionText;
    [SerializeField]
    GameObject worldAnchor;
    [SerializeField]
    GameObject airplainManipulator;
    [SerializeField]
    GameObject leftHandMenu;

    [Header("Used only when running in editor")]
    [SerializeField]
    GameObject validateButton;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
        photonView = PhotonView.Get(this);

        hostnameText.GetComponent<TextMeshPro>().text = SystemInfo.deviceName;
        worldAnchor.SetActive(false);
    }

    #region Send photon view RPC methods

    public void SendWorldAnchorPlaced()
    {   
        photonView.RPC("WorldAnchorPlaced", RpcTarget.Others);
        leftHandMenu.SetActive(false);
        airplainManipulator.SetActive(false);
        instructionText.SetActive(false);
    }

    #endregion

    #region Photon RPCs

    /// <summary>
    /// Room ready
    /// </summary>
    [PunRPC]
    public void RoomReady()
    {
        Debug.Log("[PhotonView2RPC] - RoomReady");
        if (photonView != null)
        {
            Debug.Log("=> Telling master that we are connected");
            photonView.RPC("HololensConnected", RpcTarget.Others);
        }
    }

    /// <summary>
    /// Display the hostname on the HoloLens
    /// </summary>
    [PunRPC]
    public void DisplayHostname()
    {
        Debug.Log("[PhotonView2RPC] - DisplayHostname");
        if (hostnameText != null)
        {
            hostnameText.SetActive(true);
        }
    }

    /// <summary>
    /// CAlled by another hololens device when it is connected to the room
    /// </summary>
    [PunRPC]
    public void HololensConnected()
    {
        // not used here
    }

    /// <summary>
    /// Called by the Host when a Hololens needs to place the geometry
    /// </summary>
    [PunRPC]
    public void StartPlacement()
    {
        Debug.Log("[PhotonView2RPC] - StartPlacement");
        Debug.Log("=> Placement has been started on this hololens");

        instructionText.GetComponent<TextMeshPro>().text = "Please place the scene";
        instructionText.SetActive(true);
        hostnameText.SetActive(false);
        worldAnchor.SetActive(true);
        airplainManipulator.SetActive(true);
        leftHandMenu.SetActive(true);

        //PlaceableWorldAnchor.gameObject.SetActive(true);
        //PlaceableWorldAnchor.StartPlacing();
    }

    [PunRPC]
    public void WorldAnchorPlaced()
    {
        // not used here
    }

    /// <summary>
    /// Start simulation
    /// </summary>
    [PunRPC]
    public void StartSimulation(PhotonMessageInfo info)
    {
        Debug.Log("[PhotonView2RPC] - StartSimulation");
        instructionText.SetActive(false);
        hostnameText.SetActive(false);
    }

    /*[PunRPC]
    public void StartStreamingPhotoCapture(PhotonMessageInfo info)
    {
        Debug.Log("StartStreamingPhotoCapture");
        // StartCoroutine(SendImagesToModerator(info.Sender));
    }*/

   [PunRPC]
    public void HololensReady() { }

    [PunRPC]
    public void StartAnchorTransfer() { }

    [PunRPC]
    public void TransferAnchorRequest() { }

    [PunRPC]
    public void TriggerHelpEvent() { }

    [PunRPC]
    public void TriggerVirtualEvent() { }

    [PunRPC]
    public void StopSimulation() {}

    #endregion
}
