﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectImportantObjects : MonoBehaviour
{

    private GameObject[] importantObjects;
    private GameObject hitObject;

    public PostProcess shaderScript;
    public Material material;

    public GameObject mainCamera;

    // Start is called before the first frame update
    void Start()
    {
        importantObjects = GameObject.FindGameObjectsWithTag("ImportantObjects");

        foreach (GameObject go in importantObjects)
        {
            go.AddComponent<BoxCollider>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject go in importantObjects)
        {
            float distance = Vector3.Distance(go.transform.position, transform.position);
            if (distance < 2.0)
            {
                int layerMask = ~(1 << 9);
                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, layerMask))
                {
                    if (hit.transform.gameObject.Equals(go))
                    {
                        shaderScript.enabled = false;
                        hitObject = go;
                    }
                    else
                    {
                        EnableScript(go);
                    }
                } else
                {
                    EnableScript(go);
                }
            } else
            {
                EnableScript(go);
            }
        }
    }

    private void EnableScript(GameObject go)
    {
        if (hitObject != null)
        {
            if (hitObject.Equals(go))
            {
                shaderScript.enabled = true;
                hitObject = null;
            }
        }
    }
}
