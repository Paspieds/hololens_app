﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    private string itemName;

    public Item(string itemName)
    {
        this.itemName = itemName;
    }

    public string GetName()
    {
        return itemName;
    }
}
