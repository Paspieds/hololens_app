﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillDynamicMenu : MonoBehaviour
{

    public GameObject ButtonCollection;
    public GameObject quad;
    public GameObject backButton;
    public GameObject buttonPrefab;
    public GameObject DynamicMenu;

    private Vector3 quadBaseScale;
    private Vector3 invertBackButtonTranslation;

    private float offset = 0.04f;
    private float[] starts = new float[4] { 0.0f, -0.02f, -0.04f, -0.06f };

    // Start is called before the first frame update
    void Start()
    {
        DynamicMenu.SetActive(false);
        quadBaseScale = quad.transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FillMenuWithOpenable(Openable openable)
    {

        int whichXStart = openable.size() - 1;
        float yStart = 0;

        if (openable.size() <= 4)
        {
            quad.transform.localScale = new Vector3(quad.transform.localScale.x + 0.08f * whichXStart, quad.transform.localScale.y, quad.transform.localScale.z);

            backButton.transform.Translate(new Vector3(0.02f * whichXStart, 0, 0));
            invertBackButtonTranslation = new Vector3(0.02f * whichXStart, 0, 0);

        }
        else
        {
            whichXStart = 3;

            int nbLines = openable.size() / 4;

            if (openable.size() % 4 != 0)
            {
                nbLines += 1;
            }

            for(int i = 0; i < nbLines - 1; i++)
            {
                yStart -= 0.02f;
            }

            quad.transform.localScale = new Vector3(quad.transform.localScale.x + 0.08f * whichXStart, quad.transform.localScale.y + 0.04f * (nbLines - 1), quad.transform.localScale.z);

            backButton.transform.Translate(new Vector3(0.02f * whichXStart, 0, 0));
            invertBackButtonTranslation = new Vector3(0.02f * whichXStart, 0, 0);
        }

        float xStart = starts[whichXStart];
        int line = 0;
        int column = 0;
        for (int i = 0; i < openable.size(); i++)
        {
            if (i % 4 == 0 && i != 0)
            {
                line += 1;
                column = 0;
            }

            GameObject button = Instantiate(buttonPrefab, new Vector3(quad.transform.position.x, quad.transform.position.y, quad.transform.position.z), Quaternion.identity);
            button.transform.rotation = quad.transform.rotation;
            button.transform.Translate(Vector3.back / 70);

            button.transform.Translate(new Vector3(xStart + offset * column, -(offset * line) - yStart, 0));

            button.transform.parent = ButtonCollection.transform;
            button.layer = 9;

            column += 1;
        }
    }

    public void ResetDynamicMenu()
    {
        quad.transform.localScale = quadBaseScale;

        foreach (Transform child in ButtonCollection.transform)
        {
            Destroy(child.gameObject);
        }

        backButton.transform.Translate(-invertBackButtonTranslation);
    }
}
