﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Openable
{
    private string objectName;

    private List<Item> items;

    public Openable(string objectName, List<Item> items)
    {
        this.objectName = objectName;
        this.items = items;
    }

    public string GetName()
    {
        return objectName;
    }

    public Item GetItem(int index)
    {
        return items[index];
    }

    public int size()
    {
        return items.Count;
    }
}
