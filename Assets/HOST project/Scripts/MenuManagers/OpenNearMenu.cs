﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenNearMenu : MonoBehaviour
{

    public GameObject nearMenu;
    public GameObject DynamicMenu;

    public Camera mainCamera;

    private GameObject objectHit = null;

    private List<Openable> openables = new List<Openable>();

    private bool inventoryOpened = false;

    private void Start()
    {
        nearMenu.SetActive(false);

        GameObject[] importantObjects = GameObject.FindGameObjectsWithTag("ImportantObjects");

        foreach (GameObject go in importantObjects)
        {
            List<Item> items = new List<Item>();
            items.Add(new Item("salut"));
            items.Add(new Item("tupu"));
            items.Add(new Item("ca va"));
            items.Add(new Item("ca va"));
            /*items.Add(new Item("ca va"));
            items.Add(new Item("ca va"));
            items.Add(new Item("ca va"));
            items.Add(new Item("ca va"));
            items.Add(new Item("ca va"));*/
            openables.Add(new Openable(go.name, items));
        }
    }

    void Update()
    {
        if (!inventoryOpened)
        {
            int layerMask = 1 << 8;
            RaycastHit hit;
            // Does the ray intersect any objects in OpenObject layer
            if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hit, Mathf.Infinity, layerMask))
            {
                float distance = Vector3.Distance(hit.transform.position, mainCamera.transform.position);
                if (distance < 1.0)
                {
                    nearMenu.SetActive(true);
                    objectHit = hit.transform.gameObject;
                }
                else
                {
                    nearMenu.SetActive(false);
                    objectHit = null;
                }
            }
            else
            {
                nearMenu.SetActive(false);
                objectHit = null;
            }
        }
    }

    public void OnClickOpenMenu()
    {
        if (objectHit != null)
        {
            foreach(Openable op in openables)
            {
                if (op.GetName() == objectHit.name)
                {
                    Debug.LogError(op.GetName());
                    GetComponent<FillDynamicMenu>().FillMenuWithOpenable(op);
                    DynamicMenu.SetActive(true);
                    inventoryOpened = true;
                    nearMenu.SetActive(false);
                }
            }
        }
    }

    public void OnCloseMenu()
    {
        DynamicMenu.SetActive(false);
        nearMenu.SetActive(true);
        inventoryOpened = false;

        GetComponent<FillDynamicMenu>().ResetDynamicMenu();
    }
}
