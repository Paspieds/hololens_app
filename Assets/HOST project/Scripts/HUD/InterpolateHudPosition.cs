﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterpolateHudPosition : MonoBehaviour
{
    public Transform hudAnchor;

    public float marginX = 0.3f;
    public float marginY = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        // HUD position starts at the anchor
        transform.position = hudAnchor.position;
        transform.rotation = hudAnchor.rotation;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float lerpSpeed = 5f;
        float distanceFromCamera = 2f;

        // Find the perpendicular plane from the camera view at fixed distance
        var planeCenter = hudAnchor.position;
        var planeNormal = hudAnchor.forward;

        var linePoint = transform.position;
        var lineDirection = Vector3.Normalize(transform.position - (planeCenter - distanceFromCamera * planeNormal));

        // Project the text position on the plane
        float a = Vector3.Dot((planeCenter - linePoint), planeNormal);
        float b = Vector3.Dot(lineDirection, planeNormal);

        float x = 0f;
        var intersect = new Vector3();
        if (b == 0f)
        {
            intersect = linePoint;
        }
        else
        {
            x = a / b;
            intersect = x * lineDirection + linePoint;
        }

        //Debug.DrawRay(planeCenter, planeNormal);
        //Debug.DrawRay(linePoint, lineDirection);


        // Lerp the text towards the intersection with the plane, to keep it at roughly the same distance every time
        transform.position += (intersect - transform.position) * lerpSpeed * Time.deltaTime;

        // Find the distance between the intersection and the textCenter
        float distanceX = Math.Abs(Vector3.Distance(new Vector3(planeCenter.x, 0, planeCenter.z), new Vector3(intersect.x, 0, intersect.z)));
        float distanceY = Math.Abs(Vector3.Distance(new Vector3(0, planeCenter.y, 0), new Vector3(0, intersect.y, 0)));

        // If distance is greater than the margin, we move towards the center
        if (distanceX > marginX)
        {
            // Find the maximum position we can move to
            var position = planeCenter + (new Vector3(intersect.x, 0, intersect.z) - new Vector3(planeCenter.x, 0, planeCenter.z)).normalized * marginX;

            Debug.DrawRay(position, (position - transform.position));

            transform.position += (position - transform.position) * lerpSpeed * Time.deltaTime;
        }
        if (distanceY > marginY)
        {
            // Find the maximum position we can move to
            var position = planeCenter + (new Vector3(0, intersect.y, 0) - new Vector3(0, planeCenter.y, 0)).normalized * marginY;

            Debug.DrawRay(position, (position - transform.position));

            transform.position += (position - transform.position) * lerpSpeed * Time.deltaTime;
        }

        // Apply the rotation of the camera (without the z axis)
        transform.eulerAngles = new Vector3(hudAnchor.eulerAngles.x, hudAnchor.eulerAngles.y, 0);
    }
}
